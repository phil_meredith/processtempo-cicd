import json
f = open("version.txt", encoding="utf-8")
file = open("./version.js", "w")
dictionary = {}
for x in f:
    substring = x.split("=")
    dictionary[substring[0]] = substring[1].replace('\n', '')
jsonFile = 'var ENV_VERSION = ' + json.dumps(dictionary) + ';'
file.write(jsonFile)
file.close()
