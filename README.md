# To run the Server

Resources in the server

- Docker
- node js V 10.00
- neo4j:3.5.12-enterprise
- mongo:4.0.13

## Description:

Each service described in the upper part corresponds to a particular container. The docker service must be installed.

## Creating a network

Before starting to build the services, it is necessary to create an internal network in docker, this in order to be able to generate communication between containers, since otherwise none of them could interact with each other.

```
docker network ls
```
Listado de las networks

```
docker network create processTempo
```

## Install mongo db:

In order to install mongo db, we must ensure that the ports are not available from the outside. We execute the following command to raise an instance of mongo db:

```
docker run --net processTempo --restart always --name process_tempo_mongo -p 127.0.0.1:27017:27017 -d mongo:4.2.0
```

## Install neo4j

The ** processtempo-neo4j ** repository must be downloaded.

In the root directory of the server we create two folders that we will use as volumes. (System images, import data csv)

Name of folders:
- neoData
- neoImport

Once you have entered the root directory of the project, execute the following command:

```
sudo  docker run --net processTempo -dti --name neo4jprocesstempo2 --restart always -p 7474:7474 -p 7687:7687  --env "NEO4J_dbms_memory_heap_max__size=15G" -v /root/neoData:/data -v /root/neoImport:/var/lib/neo4j/import "neo4jprocesstempo:2.0"
```