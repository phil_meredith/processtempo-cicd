# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://github.com/mokkapps/changelog-generator-demo/compare/v1.0.0...v1.1.0) (2021-04-13)


### Features

* initial feature commit ([0f02d56](https://github.com/mokkapps/changelog-generator-demo/commits/0f02d567d7f4e48f51a0984aa8c9b52ef4f0cba7))

## 1.0.0 (2021-04-13)
