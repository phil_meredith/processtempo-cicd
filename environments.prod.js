var ENV_VARS = {
  ANGULAR_API_URL: "//dev.processtempo.com:8080/api",
  ANGULAR_CLIENT_ID: "urn:processtempo:clientid:web_dev2ptopenshift4:qa",
  ANGULAR_RESOURCE: "urn:processtempo:resource:web_dev2ptopenshift4:qa",

  ANGULAR_ADFS: "false",
  ANGULAR_ISSUER: "https://corpqa.sts.ford.com/adfs/services/trust",
  ANGULAR_USER_INFO_ENDPOINT: "https://corpqa.sts.ford.com/adfs/userinfo",
  ANGULAR_LOGIN_URL: "https://corpqa.sts.ford.com/adfs/oauth2/authorize",
  ANGULAR_TOKEN_ENDPOINT: "https://corpqa.sts.ford.com/adfs/oauth2/token",
  ANGULAR_LOGOUT_URL: "https://corpqa.sts.ford.com/adfs/oauth2/logout",
  THIS_IS_JUST: "A TEST",
};
